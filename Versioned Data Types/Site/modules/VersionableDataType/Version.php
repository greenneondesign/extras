<?php
	/**
	 *	An abstract class that can be extended to create a datatype for managing
	 *	audit/edit logs. The class is not marked abstract as PHP throws an E_STRICT
	 *	error when constructing an instance of an abstract class. However,
	 *	implimentors must at the least provide a redefinition of the constant:
	 *	BASE_DATA_TYPE, the value of which must be combined with 1) '_versions'
	 *	to form the name of the database table backing the concrete data type and
	 *	2) '_id' to name the column in said table that serves as the foreign key
	 *	into the table backing the base data type.
	 */
	class Version {
		const BASE_DATA_TYPE = '';
		
		public $id;	/** the id of this Version in the database */
		public $versionable_id;	/** the id of the thing whose versions are being tracked */
		
		// audit trail stuff
		public $created_by;
		public $created_date;
		
		/**
		 *	build a new Version object
		 */
		public function __construct($id, $versionable_id, $created_by, $created_date) {
			$this->id = $id;
			$this->versionable_id = $versionable_id;
			$this->created_by = $created_by;
			$this->created_date = $created_date;
		}
		
		/**
		 *	Pull a list of all Versions from the db. Note: this does not include the
		 *	current version.
		 *
		 *	@param $id - the id of the object, the previous versions of which are
		 *		sought
		 *	@return an array of Version objects which constitue the edit log for the
		 *		object specified by the id
		 */
		public static function previous_versions($id) {
			$c = get_called_class();
			if(empty($c::BASE_DATA_TYPE)) {
				throw new \BadRequestException();
			}
			$connection = \DB::getConnection();
			$sql = 'SELECT id, created_by, created_date FROM ' . $c::BASE_DATA_TYPE . '_versions WHERE ' . $c::BASE_DATA_TYPE  . '_id = :id ORDER BY created_date DESC';
			$query = $connection->prepare($sql);
			$query->bindParam(':id', $id);
			$versions = array();
			$user_cache = array();
			if($query->execute()) {
				while($row = $query->fetch(\PDO::FETCH_ASSOC)) {
					if(array_key_exists($row['created_by'], $user_cache)) {
						$creator = $user_cache[$row['created_by']];
					} else {
						$creator = \User\User::load($row['created_by']);
						$user_cache[$row['created_by']] = $creator;
					}
					
					$version = new Version($row['id'], $id, $creator, $row['created_date']);
					$versions[] = $version;
				}
			} else { 
				\Notification::error($query->errorInfo()[2]);
			}
			return $versions;
		}
	}
?>