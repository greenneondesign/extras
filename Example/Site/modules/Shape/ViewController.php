<?php
	namespace Shape;
	
	include_once('Model.php');

	class ViewController implements \ViewController {
		/*
		Can name and visible be class variables? also need to implement a view controller
		interface to insure that automatic exists
		*/
		public static function _navigation_slug() {
			return '<i class="fa fw fa-cubes"></i> Shapes';
		}
		
		public static function _visible() {
			// uncomment if only visible to admins
//			if(empty($_SESSION['user']) || !$_SESSION['user']->has_permission('admin')) {
//				return false;
//			}

			// uncomment if only visible to authenticated users
//			if(empty($_SESSION['user'])) {
//				return false;
//			}			
			return true;
		}

		public static function automatic() {
			ViewController::index();
		}
		
		public static function index() {
			// uncomment to restrict to authenticated users
//			if(empty($_SESSION['user'])) {
//				throw new \NotAuthorizedException();
//			}

			// uncomment to restrict to admin users
//			if(empty($_SESSION['user']) || !$_SESSION['user']->is_admin) {
//				throw new \NotAuthorizedException();
//			}
			
			$shapes = Shape::all();
			\Theme::theme()->render('views/list.php', array('shapes' => $shapes));
		}
		
		public static function view($id = NULL) {
			if(is_null($id)) {
				throw new \BadRequestException();
			}
			
			$shape = Shape::load($id);
			\Theme::theme()->render('views/view.php', array('shape' => $shape));
		}
		
		public static function create() {
			if(empty($_SESSION['user']) || !$_SESSION['user']->is_admin) {
				throw new \NotAuthorizedException();
			}
			
			if(!empty($_POST)) { // do we have input?
				$shape = new Shape();
				if($shape->create()) {
					$shape->save();
					\Application::application()->redirect('/shape/view/' . $shape->id);
				} else {
					// create failed likely due to invalid input... give the user a chance to correct
					\Theme::theme()->render('views/create.php', array('shape' => $shape));
				}
			} else {
				\Theme::theme()->render('views/create.php');
			}
		}
		
		public static function update($id = NULL) {
			if(empty($_SESSION['user']) || !$_SESSION['user']->is_admin) {
				throw new \NotAuthorizedException();
			}
			
			if(is_null($id)) {
				throw new \BadRequestException();
			}
			
			if(!empty($_POST)) { // do we have input?
				$shape = Shape::load($id);
				if($shape->update()) {
					$shape->save();
					$shape = Shape::all();
					\Application::application()->redirect('/shape/view/' . $id);
				} else {
					// Notify of error
					\Theme::theme()->render('views/update.php', array('shape' => $shape));
				}
			} else {
				$shape = Shape::load($id);
				if($shape) {
					\Theme::theme()->render('views/update.php', array('shape' => $shape));
				} else {
					// Notify bad user id
					\Application::application()->redirect('/shape/');
				}
			}
		}
		
		public static function delete($id = NULL) {
			if(empty($_SESSION['user']) || !$_SESSION['user']->is_admin) {
				throw new \NotAuthorizedException();
			}
			
			if(is_null($id)) {
				throw new \BadRequestException();
			}
			
			$shape = Shape::load($id);
			if($shape->delete()) {
				\Application::application()->redirect('/shape/');
			} else {
				//failed to delete CSRF issue?
				\Application::application()->redirect('/shape/');
			}
		}
	}
?>