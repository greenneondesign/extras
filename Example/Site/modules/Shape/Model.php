<?php
	namespace Shape;

	/**
	 *	A Shape is nothing more than a string plus metadata
	 */
	class Shape {
		public $id;
		public $name;
		
		// audit trail
		public $created_by;
		public $created_date;
		public $last_updated_by;
		public $last_updated_date;

		/**
		 *	Build an invalid Shape. PHP does not allow constructor overloading so
		 *	we will default to something that is obviously broken and provide a
		 *	factory method for constructing instances from data.
		 */
		public function __construct() {
			$this->id = -1;
			$this->name = '';
			$this->created_by = NULL;
			$this->created_date = '';
			$this->last_updated_by = NULL;
			$this->last_updated_date = '';
		}
		
		/**
		 *	Creates a Shape from data
		 */
		private static function build($id, $name, $created_by, $created_date, $last_updated_by, $last_updated_date) {
			$shape = new Shape();
			$shape->id = $id;
			$shape->name = $name;
			
			$shape->created_by = $created_by;
			$shape->created_date = $created_date;
			$shape->last_updated_by = $last_updated_by;
			$shape->last_updated_date = $last_updated_date;
			
			return $shape;
		}

		/**
		 *	Pull a list of all Shapes from the db
		 */
		public static function all($filters = NULL) {
			$connection = \DB::getConnection();
			$sql = 'SELECT id, name, created_by, created_date, last_updated_by, last_updated_date FROM shapes ORDER BY name';
			$query = $connection->prepare($sql);
			$shapes = array();
			$user_cache = array();
			if($query->execute()) {
				while($row = $query->fetch(\PDO::FETCH_ASSOC)) {
					if(array_key_exists($row['created_by'], $user_cache)) {
						$creator = $user_cache[$row['created_by']];
					} else {
						try {
							$creator = \User\User::load($row['created_by']);
						} catch(\NotAuthorizedException $nae) {
							$creator = NULL;
						}
						$user_cache[$row['created_by']] = $creator;
					}
					if(array_key_exists($row['last_updated_by'], $user_cache)) {
						$updater = $user_cache[$row['last_updated_by']];
					} else {
						try {
							$updater = \User\User::load($row['last_updated_by']);
						} catch(\NotAuthorizedException $nae) {
							$updater = NULL;
						}
						$user_cache[$row['last_updated_by']] = $updater;
					}
					$shape = Shape::build($row['id'], $row['name'], $creator, $row['created_date'], $updater, $row['last_updated_date']);
					$shapes[] = $shape;
				}
			} else {
				\Notification::log('Error: ' . $query->errorInfo()[2]);
			}
			return $shapes;
		}
		
		/**
		 *	Loads a specific Shape from the DB
		 */
		public static function load($id) {
			if(is_null($id))	// we may be doing a lookup on a foreign key that is not yet set... this is not an error
				return NULL;
			
			$connection = \DB::getConnection();
			$sql = 'SELECT id, name, created_by, created_date, last_updated_by, last_updated_date FROM shapes WHERE id = :id';
			$query = $connection->prepare($sql);
			if($query->execute(array('id' => $id))) {
				if($row = $query->fetch(\PDO::FETCH_ASSOC)) {
					try {
						$creator = \User\User::load($row['created_by']);
					} catch(\NotAuthorizedException $nae) {
						$creator = NULL;
					}
					if($row['last_updated_by'] === $row['created_by']) {
						$updater = $creator;
					} else {
						try {
							$updater = \User\User::load($row['last_updated_by']);
						} catch(\NotAuthorizedException $nae) {
							$updater = NULL;
						}
					}
					return Shape::build($row['id'], $row['name'], $creator, $row['created_date'], $updater, $row['last_updated_date']);
				} else {
					\Notification::log('Shape:' . $id . ' could not be found');
					return NULL;
				}
			} else {
				\Notification::log('Error: Shape could not be found');
				return NULL;
			}
		}
		
		private function validateShapeFromFormFields() {
			$retval = true;
			
			if(!isset($_POST['name']) || empty($_POST['name'])) {
				\Notification::log("A name must be provided for the shape");
				$retval = false;
			} else {
				$this->name = strip_tags($_POST['name']);
			}

			return $retval;
		}
		
		/**
		 *	Saves a newly created or modified Shape to the database
		 */
		public function save() {
			// Allow only admins to delete Shapes
			if(!\User\User::is_user_authenticated() || !\User\User::get_authenticated_user()->has_permission('admin')) {
				throw new \NotAuthorizedException();
			}
			
			$connection = \DB::getConnection();
			if($this->id < 0) { // a new section SQL::insert
				$sql = 'INSERT INTO shapes(name, created_by, last_updated_by) VALUES(:name, :created_by, :last_updated_by)';
				$query = $connection->prepare($sql);
				$this->created_by = \User\User::get_authenticated_user()->id;
				$query->bindParam(':created_by', $this->created_by->id);
			} else {	// an existing section SQL::update
				$sql = 'UPDATE shapes SET name = :name, last_updated_by = :last_updated_by, last_updated_date = CURRENT_TIMESTAMP WHERE id = :id';
				$query = $connection->prepare($sql);
				$query->bindParam(':id', $this->id);
			}

			$this->last_updated_by = \User\User::get_authenticated_user();
			$query->bindParam(':last_updated_by', $this->last_updated_by->id);
			$query->bindParam(':name', $this->name);
			
			if($query->execute()) {
				if($this->id < 0) {
					if($connection->driver === 'pgsql') {
						$this->id = $connection->lastInsertId('shapes_id_seq');
					} else {
						$this->id = $connection->lastInsertId();
					}
					\Notification::log('Shape Created');
					$this->created_date = date();
				} else {
					\Notification::log('Shape Saved');
				}
				$this->last_updated_date = date();
			} else {
				\Notification::log('Error: ' . $query->errorInfo()[2]);
			}
		}

		public function create() {		
			return Shape::validateShapeFromFormFields();
		}
		
		public function update() {
			return Shape::validateShapeFromFormFields();
		}
		
		public function delete() {
			// Allow only admins to delete Shapes
			if(!\User\User::is_user_authenticated() || !\User\User::get_authenticated_user()->has_permission('admin')) {
				throw new \NotAuthorizedException();
			}
			
			$connection = \DB::getConnection();
			$sql = 'DELETE FROM shapes WHERE id = :id';
			$query = $connection->prepare($sql);
			$query->bindParam(':id', $this->id);
			
			if($query->execute()) {
				\Notification::log('Shape Destroyed');
				return true;
			}
			return false;
		}
	}
?>