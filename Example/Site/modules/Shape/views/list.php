			<article>
				<div>
					<h1>Shapes</h1>
					<table>
						<thead>
							<tr><th>Name</th></tr>
						</thead>
						<tbody>
<?php if(count($shapes)>0): ?>
<?php foreach($shapes as $shape): ?>
							<tr>
								<td><a href="/shape/view/<?=$shape->id?>" title="View Shape"><?=$shape->name?></a></td>
							</tr>
<?php endforeach; ?>
<?php else: ?>
<?php if(!empty($_SESSION['user']) && $_SESSION['user']->is_admin): ?>
							<tr><td colspan="2">No shapes found. Would you like to add one now?</td></tr>
<?php else: ?>
							<tr><td>No shapes found.</td></tr>
<?php endif; ?>
<?php endif; ?>
						</tbody>
<?php if(!empty($_SESSION['user']) && $_SESSION['user']->is_admin): ?>
						<tfoot>
							<tr>
								<td><div class="button-group"><a class="default" href="/shape/create/">New Shape</a></div>
							</tr>
						</tfoot>
<?php endif; ?>
					</table>
				</div>
			</article>
