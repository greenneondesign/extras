			<ul class="breadcrumbs">
				<li><a href="/shape/">Shapes</a></li>
				<li>Edit <?=$shape->name ?></li>
			</ul>
			<article>
				<div>
					<h1><?=$shape->name ?></h1>
					<form method="post">
						<div class="form-field text"><label for="name">Name:</label><input type="text" id="name" name="name" value="<?=isset($shape) ? $shape->name : '' ?>" required></div>
						<div class="button-group">
							<a href="/shape/">Cancel</a><button type="submit">Save</button>
						</div>
					</form>
				</div>
			</article>
