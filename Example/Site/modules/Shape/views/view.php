			<ul class="breadcrumbs">
				<li><a href="/shape/">Shapes</a></li>
				<li><?=$shape->name ?></li>
			</ul>
			<article>
				<div>
					<h1><?=$shape->name ?></h1>
					<dl>
						<dt>Created</dt><dd>by: <?=$shape->created_by->getDisplayName() ?> on <?=$shape->created_date ?></dd>
						<dt>Updated</dt><dd>by: <?=$shape->last_updated_by->getDisplayName() ?> on <?=$shape->last_updated_date ?></dd>
					</dl>
<?php if(!empty($_SESSION['user']) && $_SESSION['user']->is_admin): ?>
					<div class="button-group">
						<a href="/shape/delete/<?=$shape->id ?>">Delete</a><a href="/shape/update/<?=$shape->id ?>">Edit</a>
					</div>
<?php endif; ?>
				</div>
			</article>
