			<ul class="breadcrumbs">
				<li><a href="/shape/">Shapes</a></li>
				<li>New Shape</li>
			</ul>
			<article>
				<div>
					<h1>New Shape</h1>
					<form method="post">
						<div class="form-field text"><label for="name">Name:</label><input type="text" id="name" name="name" value="<?=isset($shape) ? $shape->name : '' ?>"></div>
						<div class="button-group">
							<a href="/shape/">Cancel</a><button type="submit">Create</button>
						</div>
					</form>
				</div>
			</article>
