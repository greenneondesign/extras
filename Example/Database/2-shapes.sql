--	Portability...
--	Postgres runs as is
--	MySQL
--		issue: Foreign key type mismatch between id, created_by and last_update_by
--		solution: Either
--				a) change the type of id to integer Auto Increment
--				b) change the type of created_by and last_updated_by to BIGINT UNSIGNED (preferred)
CREATE TABLE  IF NOT EXISTS shapes (
	id serial PRIMARY KEY,
	name text NOT NULL,
	created_by integer NOT NULL,
	created_date timestamp DEFAULT now() NOT NULL,
	last_updated_by integer NOT NULL,
	last_updated_date timestamp DEFAULT now() NOT NULL
);

ALTER TABLE users
	ADD CONSTRAINT fk_shapes_created_by FOREIGN KEY (created_by) REFERENCES users(id);

ALTER TABLE users
	ADD CONSTRAINT fk_shapes_last_updated_by FOREIGN KEY (last_updated_by) REFERENCES users(id);

INSERT INTO shapes (name, created_by, last_updated_by) VALUES
('circle', 0, 0),
('square', 0, 0),
('triangle', 0, 0),
('hexagon', 0, 0);